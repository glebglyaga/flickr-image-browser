import PropTypes from 'prop-types';
import React, { useRef, useState } from 'react';

import { SPINNER } from '../assets';
import Logger from '../../util/logger';

export function CarouselItem({ current, imageUrl }) {
  const className = current === true
    ? 'carousel-item carousel-item--current'
    : 'carousel-item';
  const [ loaded, setLoaded ] = useState(false);
  const [ loading, setLoading ] = useState(false);
  const [ src, setSrc ] = useState('');
  const spinner = loading
    ? <div className='spinner'>{SPINNER}</div>
    : null;

  function loadHandler(event) {
    DEBUG && Logger.createTaggedLoggers('CarouselItem').log(event.target.src, 'loaded');

    setLoaded(true);
    setLoading(false);
  }

  if (current === true && loaded === false && loading === false) {
    setLoading(true);
    setSrc(imageUrl);
  }

  return (
    <div className={className}>
      <img src={src} onLoad={loadHandler}/>
      {spinner}
    </div>
  );
}

CarouselItem.propTypes = {
  current:  PropTypes.bool,
  imageUrl: PropTypes.string,
};