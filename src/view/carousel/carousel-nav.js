import PropTypes from 'prop-types';
import React from 'react';
import { PREV, NEXT } from '../assets';

export function CarouselNav({ setPrevHandler, setNextHandler }) {
  return (
    <div className="carousel-nav">
      <div className="carousel-nav__button carousel-nav__button--prev" onClick={setPrevHandler}>
        {PREV}
      </div>
      <div className="carousel-nav__button carousel-nav__button--next" onClick={setNextHandler}>
        {NEXT}
      </div>
    </div>
  );
}

CarouselNav.propTypes = {
  setPrevHandler: PropTypes.func,
  setNextHandler: PropTypes.func,
};