import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { CarouselItem } from './carousel-item';
import { CarouselNav } from './carousel-nav';

export function ImagesCarousel({ images }) {
  const [ currentIndex, setCurrentIndex ] = useState(0);

  function changeCurrentIndex(value) {
    const newCurrentIndex = currentIndex + value;

    if (newCurrentIndex < 0 || newCurrentIndex > images.length - 1) {
      return currentIndex;
    } else {
      setCurrentIndex(newCurrentIndex);
    }
  }

  return (
    <div className="images-carousel">

      <div className="images-container">
        {images.map(({ id, url }, i) => <CarouselItem key={id} imageUrl={url} current={currentIndex === i}/>)}
      </div>
      <CarouselNav
        setPrevHandler={() => changeCurrentIndex(-1)}
        setNextHandler={() => changeCurrentIndex(+1)}
      />
    </div>
  );
}

ImagesCarousel.propTypes = {
  images: PropTypes.arrayOf(PropTypes.object)
};