import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { getImages } from '../model/selectors/selectors';
import { ImagesCarousel } from './carousel/images-carousel';
import Logger from '../util/logger';

function RootLayout({ images }) {
  DEBUG && Logger.createTaggedLoggers('RootLayout').log(images);

  // function inputChangeHandler(event) {
  //   console.warn(event.target.value);
  // }

  return (
    <div className="root-layout">
      {/* <input type="text" onChange={inputChangeHandler}/> */}
      <ImagesCarousel images={images}/>
    </div>
  );
}

export default connect(state => {
  return {
    images: getImages(state)
  };
})(RootLayout);

RootLayout.propTypes = {
  images: PropTypes.arrayOf(PropTypes.object)
};