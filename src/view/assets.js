import React from 'react';

export const PREV = (
  <svg viewBox="0 0 30 30" version="1.1" xmlns="http://www.w3.org/2000/svg">
    <g id="icon-play" fill="#FFFFFF" fillRule="evenodd">
      <path d="M20,8 L20,22 L11,15 L20,8 Z"></path>
    </g>
  </svg>
);

export const NEXT = (
  <svg viewBox="0 0 30 30" version="1.1" xmlns="http://www.w3.org/2000/svg">
    <g id="icon-play" fill="#FFFFFF" fillRule="evenodd">
      <path d="M10,8 L10,22 L21,15 L10,8 Z"></path>
    </g>
  </svg>
);

export const SPINNER = (
  <svg viewBox="0 0 68 68" version="1.1" xmlns="http://www.w3.org/2000/svg">
    <g id="spinner" fill="#FFFFFF" fillRule="evenodd">
      <path d="M34,68 C52.7776815,68 68,52.7776815 68,34 C68,15.2223185 52.7776815,0 34,0 C15.2223185,0 0,15.2223185 0,34 C0,52.7776815 15.2223185,68 34,68 Z M34,65 C51.1208272,65 65,51.1208272 65,34 C65,16.8791728 51.1208272,3 34,3 C16.8791728,3 3,16.8791728 3,34 C3,51.1208272 16.8791728,65 34,65 Z" id="donut" fillOpacity="0.2"></path>
      <path d="M68,34 C68,15.2223185 52.7776815,0 34,0 L34,3 C51.1208272,3 65,16.8791728 65,34 L68,34 Z" id="segment"></path>
    </g>
  </svg>
);