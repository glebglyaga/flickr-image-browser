import * as ActionTypes from './action-types';

export function error(state, action) {
  switch (action.type) {
    case ActionTypes.ERROR_DID_OCCUR:
      return action.payload;
    default:
      return state;
  }
}

export function images(state, action) {
  switch (action.type) {
    case ActionTypes.IMAGES_DID_RECIEVE:
      return [ ...action.payload ];
    default:
      return state;
  }
}