import { applyMiddleware, createStore } from 'redux';
import reduxThunkMiddleware from 'redux-thunk';

import * as ActionTypes from './action-types';
import FsaReducer from '../util/fsa-reducer';
import * as Reducers from './reducers';

export function createReduxStore(initialState) {
  const reducer = new FsaReducer(Reducers, ActionTypes).create();
  // const reducer = (state, action) => ({
  //   error: Reducers.error(state.error, action),
  // });

  return createStore(reducer, initialState, applyMiddleware(reduxThunkMiddleware));
}

export function getInitialState() {
  let initialScheme = {
    error:  null,
    images: []
  };

  return initialScheme;
}
