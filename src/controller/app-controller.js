import React from 'react';
import ReactDom from 'react-dom';
import { Provider } from 'react-redux';

import { createReduxStore, getInitialState } from '../model/redux-store';
import Logger from '../util/logger';
import RootLayout from '../view/root-layout';
import { getError } from '../model/selectors/selectors';
import storeObserver from '../util/store-observer';
import { fetchImages } from '../action/actions';

export default class AppController {
  constructor(config) {
    this.logger = Logger.createTaggedLoggers('AppController');
    this.view = config.view;
    this.store = createReduxStore(getInitialState());
    this.storeObservers = [];

    DEBUG && this.logger.log('App initialized with config:', config);

    this.addListeners();
    this.bootstrap();
    this.render();
  }

  addListeners() {
    this.addObserver(getError, error => {
      if (error) {
        this.logger.error('Error: ', error);
      }
    });
  }

  addObserver(selector, listener) {
    this.storeObservers.push(storeObserver(this.store, selector, listener));
  }

  bootstrap() {
    // Seattle lat & lon
    this.store.dispatch(fetchImages({ lat: 47.6, lon: -122.3 }));
  }

  dispose() {
    if (this.storeObservers !== null) {
      this.storeObservers.forEach(unSub => unSub());
      this.storeObservers = null;
    }

    this.store = null;
  }

  render() {
    ReactDom.render(
      <Provider store={this.store}>
        <RootLayout/>
      </Provider>,
      this.view
    );
  }
}