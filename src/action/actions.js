import * as ActionTypes from '../model/action-types';
import * as Constants from '../model/constants';
import { getData } from '../util/utils';

export function fetchImages({ perPage = 10, page = 1, lat, lon }) {
  return dispatch => {
    getData(`https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=${Constants.API_KEY}&lat=${lat}&lon=${lon}&accuracy=11&per_page=${perPage}&page=${page}&format=json&nojsoncallback=1`)
      .then(
        searchResponse => searchResponse.photos.photo.map(
          ({ farm, server, id, secret }) => ({ id, url: `https://farm${farm}.staticflickr.com/${server}/${id}_${secret}_b.jpg` })
        ),
        err => dispatch(setError(err))
      )
      .then(result => {
        dispatch(setImages(result));
      });
  };
}

export function setError(error) {
  return {
    type:    ActionTypes.ERROR_DID_OCCUR,
    payload: error
  };
}

export function setImages(images) {
  return {
    type:    ActionTypes.IMAGES_DID_RECIEVE,
    payload: images
  };
}